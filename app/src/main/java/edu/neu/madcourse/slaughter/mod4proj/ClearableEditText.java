package edu.neu.madcourse.slaughter.mod4proj;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;


public class ClearableEditText extends LinearLayout {

    EditText editText;
    Button clearButton;

    public ClearableEditText(Context context) {
        super(context);
        setupView(context);
    }

    public ClearableEditText(Context context, AttributeSet attr){
        super(context, attr);
        setupView(context);

    }

    private void setupView(Context context) {
        // Inflate the view from the layout resource.
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.clearable_edit_text, this, true);

        // Get references to the child controls.
        editText = (EditText) findViewById(R.id.editText);
        clearButton = (Button) findViewById(R.id.clearButton);

        // Hook up the functionality
        hookupButton();
    }

    private void hookupButton() {
        clearButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                editText.setText("");
            }
        });

    }

}
