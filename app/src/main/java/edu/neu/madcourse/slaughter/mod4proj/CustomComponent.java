package edu.neu.madcourse.slaughter.mod4proj;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by ahslaughter on 2/26/17.
 */

public class CustomComponent extends View {

    public CustomComponent(Context context) {
        super(context);
    }

    public CustomComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
