package edu.neu.madcourse.slaughter.mod4proj;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MultiFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_fragment);

        // Create a new Fragment to be placed in the activity layout
        SampleFragment firstFragment = new SampleFragment();

        // In case this activity was started with special instructions from an
        // Intent, pass the Intent's extras to the fragment as arguments
        firstFragment.setArguments(getIntent().getExtras());
        firstFragment.setRetainInstance(true);

        SampleFragment secondFragment = new SampleFragment();
        secondFragment.setArguments(getIntent().getExtras());
        secondFragment.setRetainInstance(true);

        // Need to have something to hold the fragment defined in the activity XML layout
        getSupportFragmentManager().beginTransaction()
                .add(R.id.activity_multi_fragment_ll, firstFragment).commit();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.activity_multi_fragment_ll, secondFragment).commit();


    }
}
